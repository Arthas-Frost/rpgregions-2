package net.islandearth.rpgregions.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

public class RegionsEffectEvent extends Event {

	private static final HandlerList HANDLER_LIST = new HandlerList();
	private final Player player;
	private final List<String> regions;

	public RegionsEffectEvent(Player player, List<String> regions) {
		this.player = player;
		this.regions = regions;
	}

	/**
	 * The player involved in this event.
	 * @return the player involved
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets a list of all regions that will give effects to the player.
	 * @return {@link List} of regions
	 */
	public List<String> getRegions() {
		return regions;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLER_LIST;
	}

	public static HandlerList getHandlerList() {
		return HANDLER_LIST;
	}
}
