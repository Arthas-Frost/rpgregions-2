package net.islandearth.rpgregions.api;

import com.convallyria.languagy.api.language.Translator;
import com.google.gson.Gson;
import net.islandearth.rpgregions.managers.IRPGRegionsManagers;
import org.bukkit.configuration.Configuration;

import java.io.File;
import java.util.logging.Logger;

public interface IRPGRegionsAPI {

    Translator getTranslator();

    Logger getLogger();

    File getDataFolder();

    void saveResource(String path, boolean replace);

    Gson getGson();

    Configuration getConfig();

    IRPGRegionsManagers getManagers();

    boolean debug();
    
    void debug(String debug);

    boolean hasHeadDatabase();
}
