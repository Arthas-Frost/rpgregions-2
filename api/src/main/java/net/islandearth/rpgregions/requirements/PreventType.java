package net.islandearth.rpgregions.requirements;

import io.papermc.lib.PaperLib;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;

public enum PreventType {
    TELEPORT,
    PUSH,
    CANCEL;

    public void prevent(PlayerMoveEvent event) {
        if (event.getTo() == null) return;
        Player player = event.getPlayer();
        switch (this) {
            case TELEPORT -> PaperLib.teleportAsync(player, event.getFrom());
            case PUSH -> player.setVelocity(event.getTo().toVector().subtract(event.getFrom().toVector()).multiply(-3));
            case CANCEL -> event.setCancelled(true);
        }

        if (getVersionNumber() <= 17) {
            player.spawnParticle(Particle.valueOf("BARRIER"), event.getTo().getBlock().getLocation().add(0.5, 0.5, 0.5), 1);
        } else {
            player.spawnParticle(Particle.BLOCK_MARKER, event.getTo().getBlock().getLocation().add(0.5, 0.5, 0.5), 1, Material.BARRIER.createBlockData());
        }
    }

    private int getVersionNumber() {
        String[] split = Bukkit.getBukkitVersion().split("-")[0].split("\\.");
        return Integer.parseInt(split[1]);
    }
}
